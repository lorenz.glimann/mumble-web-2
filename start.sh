#!/bin/bash
sudo websockify --cert=/etc/letsencrypt/live/fachschaften.uni-goettingen.de/fullchain.pem --key=/etc/letsencrypt/live/fachschaften.uni-goettingen.de/privkey.pem --ssl-only --ssl-target --web=/home/cloud/mumble-web/dist 81 localhost:64738
